# InCore Development Environment Image
FROM registry.gitlab.com/incoresemi/dockers/docker-bsc:latest

# UPDATE / UPGRADE

ENV PATH=$PATH:/riscv/bin

RUN apt-get update

RUN DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends device-tree-compiler \
  autoconf automake autotools-dev curl python3 libmpc-dev libmpfr-dev libgmp-dev gawk \
  build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev libexpat-dev \
  python-dev python3-dev libreadline-dev openjdk-11-jdk gpg-agent


# RISCV-GNU-TOOLCHAIN
RUN git clone https://github.com/riscv-collab/riscv-gnu-toolchain.git -b 2022.02.12 && \
  mkdir /riscv && \
  cd riscv-gnu-toolchain && \
  git submodule init && \
  sed -i '/qemu/d' .gitmodules && \
  sed -i '/qemu/d' .git/config && \
  git submodule update --recursive --jobs=8 && \
  ./configure --prefix=/riscv --with-arch=rv64gc --with-abi=lp64d && \
  make -j8 1>/dev/null && \
  make clean && \
  ./configure --prefix=/riscv --with-arch=rv32gc --with-abi=ilp32d && \
  make -j8 1>/dev/null && \
  make clean && \
  cd .. && \
  rm -rf riscv-gnu-toolchain


# SPIKE
RUN git clone https://github.com/riscv-software-src/riscv-isa-sim.git -b master && \
  mkdir /spike && \
  cd riscv-isa-sim && \
  mkdir build && \
  cd build && \
  ../configure --prefix=/spike/ --enable-commitlog && \
  make -j8 1>/dev/null && \
  make install && \
  cd ../../ && \
  rm -rf riscv-isa-sim

# INCORE-SPIKE
RUN git clone https://github.com/incoresemi/riscv-isa-sim.git -b incoresemi && \
  mkdir /incspike && \
  cd riscv-isa-sim && \
  mkdir build && \
  cd build && \
  ../configure --prefix=/incspike/ --enable-commitlog && \
  make -j8 1>/dev/null && \
  make install && \
  cd ../../ && \
  rm -rf riscv-isa-sim

# Install Scala
RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | tee \ 
        /etc/apt/sources.list.d/sbt.list && \ 
    echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | tee \
        /etc/apt/sources.list.d/sbt_old.list && \ 
    curl -sL \
    "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" \
    | apt-key add && apt update && apt install sbt -y --no-install-recommends 

RUN git clone https://github.com/berkeley-abc/abc.git && \
  cd abc && make -j$(nproc) && \
  cp abc /usr/bin && \
  cd .. && \
  rm -rf abc

RUN mkdir -p /home/workdir/
WORKDIR /home/workdir/
