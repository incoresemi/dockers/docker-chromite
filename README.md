
**⚠ WARNING: This project has been moved to [new location](https://gitlab.incoresemi.com/dockers/docker-chromite.git). It will soon be archived and eventually deleted.**

# Docker File 
Details: 

- Ubuntu 20.04
- BSC version 2021.12.1
- Verilator version 4.106
- Python version 3.8.10
- RISC-V-GNU-TOOLCHAIN 11.1.0
- Spike
